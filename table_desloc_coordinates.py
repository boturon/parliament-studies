
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
from geopy.geocoders import Nominatim
import json
import time
from geopy.exc import GeocoderTimedOut

# In[2]:


data_source = "./../../../git/parlamento/data/"


# # Get all visits per parlamentarian with coordinates

# In[3]:


def get_coordinates(address):
    try:
        geolocator = Nominatim().geocode(address,timeout=10)
        if geolocator == None:
            return(["miss_loc"]*3)
        else:
            return([geolocator.address.split(',')[0], geolocator.latitude, geolocator.longitude])
    except GeocoderTimedOut:
        return(["timed_out"]*3)


# In[4]:


def get_deloc_data(source):
    
    print("Start. "+time.ctime())
    
    counter = 0

    for leg in ["07","08","09","10","11","12","13"]:
        print("Beginning of legislature "+str(int(leg))+". "+time.ctime())
        output = pd.DataFrame(index=["legDes", "depId", "depCPDes", "start_lat", "start_lon", "actId", "actDtdes1", "actLoc", "end_lat", "end_lon"])
        error_report = {"miss_deslo":{"count": 0}, "miss_loc":{"count": 0}, "timed_out":{"count": 0}}
        
        data_atividadesdeputados = json.loads(open(source+"atividade-deputado-"+leg+".json").read())
        for index_parl, parlamentarian in enumerate(data_atividadesdeputados["ArrayOfAtividadeDeputado"]["AtividadeDeputado"]):
            output_ini = [leg, parlamentarian["deputado"]["depId"]]
            
            start_pair = get_coordinates(parlamentarian["deputado"]["depCPDes"]+", Portugal")
            [output_ini.append(i) for i in start_pair]            

            temp_act = parlamentarian["AtividadeDeputadoList"]["pt_gov_ar_wsar_objectos_ActividadeOut"]
            temp_test = temp_act.get("deslocacoes", "miss_deslo")
            
            if temp_test == "miss_deslo":
                error_report["miss_deslo"]["count"] += 1
            elif type(temp_test["pt_gov_ar_wsar_objectos_ActividadesComissaoOut"]) == dict:
                output_dict = output_ini.copy()
                [output_dict.append(temp_test["pt_gov_ar_wsar_objectos_ActividadesComissaoOut"][i]) for i in ["actId", "actDtdes1"]]
                
                end_pair = get_coordinates(temp_test["pt_gov_ar_wsar_objectos_ActividadesComissaoOut"]["actLoc"].split(';')[0])
                [output_dict.append(i) for i in end_pair]            
                
                if end_pair[0] == "miss_loc":
                    error_report["miss_loc"]["count"] += 1
                elif end_pair[0] == "timed_out":
                    error_report["timed_out"]["count"] += 1
                
                counter += 1
                output[counter] = output_dict
            else:
                for each_desl in temp_test["pt_gov_ar_wsar_objectos_ActividadesComissaoOut"]:
                    output_list = output_ini.copy()
                    [output_list.append(each_desl[i]) for i in ["actId", "actDtdes1"]]
                    
                    end_pair = get_coordinates(each_desl["actLoc"].split(';')[0])
                    [output_list.append(i) for i in end_pair]
                    
                    if end_pair[0] == "miss_loc":
                        error_report["miss_loc"]["count"] += 1
                        #print(each_desl["actLoc"])
                    elif end_pair[0] == "timed_out":
                        error_report["timed_out"]["count"] += 1
                    counter += 1
                    output[counter] = output_list
            if (index_parl+1)%100==0:
                print("Finished "+ str(index_parl+1)+"th parlamentarian. "+time.ctime())
        
        output.to_csv("./table_years/table_desloc_coord_"+leg+".csv", index=False)
        print(error_report)


# In[5]:


get_deloc_data(data_source)



