
# coding: utf-8

# In[177]:


import json
import time
import pandas as pd


# In[51]:


path = "./../parlamento/data/"


# <br>

# In[111]:


df_situation = pd.DataFrame(index = ["leg", "legDeg", "depCadId", "depNomeParlamentar", "sioDs", "sioDtInicio", "sioDtFim"])
counter = 0
for leg in ["02","03","04","05","06", "07", "08","09","10", "11", "12","13"]:
    file =  json.loads(open("%sinfo-base-%s.json"% (path,leg)).read())
    deputados_info = file["Legislatura"]["Deputados"]["pt_ar_wsgode_objectos_DadosDeputadoSearch"]
    for dep_i, dep in enumerate(deputados_info):
        feature = [leg]
        feature.extend([dep.get(i, "miss_"+i) for i in ["legDes", "depCadId", "depNomeParlamentar"]])
        presence_info = dep["depSituacao"]['pt_ar_wsgode_objectos_DadosSituacaoDeputado']
        if type(presence_info) == dict:
            presence_info = [presence_info]
        for situation in presence_info:
            feature_i = feature.copy()
            feature_i.extend([situation.get(i, "miss_"+i) for i in ["sioDes", "sioDtInicio", "sioDtFim"]])
            if feature_i[-3] == "Falecido/a":
                feature_i[-1] = feature_i[-2]
            df_situation[counter] = feature_i
            counter += 1 

df_situation = df_situation.transpose()


# In[134]:


df_situation.head(2)


# In[112]:


df_situation[df_situation.sioDtFim=="miss_sioDtFim"].groupby(["leg", "sioDs"]).count()[["depCadId"]]


# <br>

# In[164]:


situation = df_situation[df_situation.leg.astype(int) < 13].copy()
situation.sioDtInicio = pd.to_datetime(situation.sioDtInicio)
situation.sioDtFim = pd.to_datetime(situation.sioDtFim)
situation["code"] = situation.sioDs.map(dict(zip(sorted(set(situation.sioDs)),list(range(1, len(set(situation.sioDs))+1)))))


# In[165]:


situation.head(5)


# In[146]:


situation.sioDs.value_counts().to_frame()


# In[154]:


situation_codes = {"situation":sorted(set(situation.sioDs)), "code":list(range(1, len(set(situation.sioDs))+1))}
pd.DataFrame(situation_codes)


# In[192]:


situation_matrix = pd.DataFrame(columns = pd.Series(sorted(set(situation.depCadId.astype(int)))).astype(str), 
                                index = pd.date_range(situation.sioDtInicio.min(), situation.sioDtFim.max())).rename_axis('Date')


# In[196]:


print("Start", time.ctime())
for index, row in situation.iterrows():
    if index%100==0:
        print(str(index)+", "+str(int(index/9086*100))+"%, "+time.ctime())
    situation_matrix.loc[row.sioDtInicio:row.sioDtFim, row.depCadId] = row.code
print("End", time.ctime())


# In[197]:


situation_matrix.to_csv("./tables/table_situations.csv", index=False)

